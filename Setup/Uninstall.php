<?php
namespace JuistdIT\CustomAttributeCustomer\Setup;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Api\AttributeRepositoryInterface as AttributeRepositoryInterface;



/**
 * @codeCoverageIgnore
 */
class Uninstall implements UninstallInterface
{

    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    protected $moduleDataSetup;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory

    ) {

        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }


    /**
     * {@inheritdoc}
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        /** @var CustomerSetup $customerSetup */
        //$customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);


        //AttributeRepositoryInterface::delete('test_jurn');

        //$customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        //$attributeSetId = $customerEntity->getDefaultAttributeSetId();

        /** @var $attributeSet AttributeSet */
        //$attributeSet = $this->attributeSetFactory->create();
        //$attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        //$customerSetup->removeAttribute(\Magento\Customer\Model\Customer::ENTITY, "test_jurn");

        $setup->endSetup();
    }
}